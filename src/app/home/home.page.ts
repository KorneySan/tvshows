import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Channel } from '../models/data.model';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  //public channels: Channel[] = [];

  constructor(
    private dataService: DataService//,
    ) {}

  ngOnInit() {
    this.dataService.getChannelsData();
    /*
    this.dataService.getChannels().then(result => {
      this.channels = result;
      //
      
      if (this.loading) {
        this.loading.dismiss();
        this.loading = null;
      }
      
      this.dataService.loadingHide();
    });
    */
  }
}
