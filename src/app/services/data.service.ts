import { Injectable, Input, OnInit, OnDestroy } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Channel, TvShow } from '../models/data.model';
import { LoadingController, Events, PopoverController } from '@ionic/angular';
import * as moment from 'moment';
import { ProgressComponent } from '../components/progress/progress.component';

@Injectable()
export class DataService implements OnInit, OnDestroy {

  @Input() loadingText: string = 'Загрузка...';
  private readonly BASE_URL = 'https://api.persik.by/v2/'
  private loading: any = null;
  private isLoadingShowing: boolean = false;
  public channels: Channel[] = [];
  private popover: any = null;
  private loadingCount: number = 0;

  constructor(
    private http: HttpClient,
    private loadingController: LoadingController,
    private events: Events,
    private popoverController: PopoverController
  ) {}

  ngOnInit(): void {
    this.events.subscribe('ionLoadingDidPresent', this.eventsHandler);
  }
  ngOnDestroy(): void {
    this.events.unsubscribe('ionLoadingDidPresent',  this.eventsHandler);
  }

  private eventsHandler(): void {
    console.log('ionLoadingDidPresent');
    this.isLoadingShowing = true;
  }

  public getChannels(loadingText?: string): Promise<Channel[]> {
    this.loadingShow('Загрузка каналов...', loadingText);
    return this.http.get<ServerChannels>(this.BASE_URL.concat('content/channels')).pipe(map(res => res.channels)).toPromise();
  }

  public async getChannelsImmediately(loadingText?: string): Promise<Channel[]> {
    this.loadingShow('Загрузка каналов...', loadingText);
    return await this.http.get<ServerChannels>(this.BASE_URL.concat('content/channels')).pipe(map(res => res.channels)).toPromise();
  }

  public getChannelsData(): void {
    this.getChannels().then(result => {
      if (result) this.channels = result;
      else this.channels = [];
      this.loadingHide();
    });
  }

  public getTvShowsById(channel_id: number, loadingText?: string): Promise<TvShow[]> {
    this.loadingShow('Загрузка передач канала...', loadingText);
    const params: HttpParams = new HttpParams().set('channels[]', channel_id.toString());
    return this.http.get<ServerTvShows>(this.BASE_URL.concat('epg/tvshows'), { params }).pipe(map(res => res.tvshows.items)).toPromise();
  }
  /*
  public getLiveChannels(channels: Channel[], loadingText?: string): Promise<LiveChannel[]> {
    this.loadingShow((!channels || 0 == channels.length)? 'Загрузка каналов и передач...' : 'Загрузка передач...', loadingText);
    
    return new Promise( resolve => {
      let liveChannels: LiveChannel[] = []; 
      if (!channels || 0 == channels.length) {
        this.getChannels().then(channels => {
          this.loadingHide();
          this.getChannelsLiveData(channels).then(serverLiveChannels => {
            liveChannels = serverLiveChannels;
          });          
        });
      }
      else {
        this.getChannelsLiveData(channels).then(serverLiveChannels => {
          liveChannels = serverLiveChannels;
        });          
      } 
      resolve(liveChannels);
    });
  }

  private getChannelsLiveData(channels: Channel[]): Promise<LiveChannel[]> {
    return new Promise(resolve => {
      let liveChannels: LiveChannel[] = [];
      const currentTime = moment().unix();
      //this.popoverShow();
      for (let i=0; i<channels.length; i++) {
        //console.log(i+1, '/', channels.length);
      
        this.getTvShowsById(channels[i].channel_id).then(tvshows => {
          const currentTvShow: TvShow = tvshows.find(tvshow => {
            return tvshow.start<currentTime && tvshow.stop>=currentTime;
          });
          if (currentTvShow) {
            const liveChannel: LiveChannel = {
              channel: channels[i],
              tvShow: currentTvShow
            };
            liveChannels.push(liveChannel);
            //console.log(liveChannels.length);
          }
        });
        if (channels.length>0) this.events.publish('progressUpdate', (i+1)/channels.length);
      }
      resolve(liveChannels);
    });
  }
  */
  public getCurrentTvShowById(channel_id: number, currentTime: number, loadingText?: string): Promise<TvShow> {
    return new Promise(resolve => {
      let currentTvShow: TvShow = null;
      this.getTvShowsById(channel_id).then(tvshows => {
        //console.log(currentTime);
        //console.log(tvshows);
        currentTvShow = tvshows.find(tvshow => {
          //return tvshow.start<currentTime && tvshow.stop>=currentTime;
          //console.log(tvshow.start, '-', currentTime, '-', tvshow.stop);
          return moment.unix(currentTime).isBetween(moment.unix(tvshow.start), moment.unix(tvshow.stop));
        });
        resolve(currentTvShow);
      });
    });
}

  private async presentLoading() {
    this.loading = await this.loadingController.create({
      message: this.loadingText,
      translucent: true,
      backdropDismiss: true
    });
    await this.loading.present();
  }

  private loadingShow(internalLoadingText: string, externalLoadingText?: string): void {
    if (this.loading) this.loadingCount++;
    else {
      if (externalLoadingText) this.loadingText = externalLoadingText;
      else {
        if (internalLoadingText) this.loadingText = internalLoadingText;
        else this.loadingText = 'Загрузка...';
      }
      this.presentLoading();
      this.loadingCount = 1;
    }
  }

  public loadingHide(): void {
    if (this.loading) {
      if (this.loadingCount>0) this.loadingCount--;
      else {
        this.loading.dismiss().then(() => {
          this.loading = null;
          this.isLoadingShowing = false;
        });
        this.loadingCount = 0;
      }
    }
    else this.loadingCount = 0;
  }

  public async loadingHideImmediately() {
    if (this.loading) {
      await this.loading.dismiss();
      this.loading = null;
      this.isLoadingShowing = false;
    }
  }

  private async presentPopover(ev: any) {
    this.popover = await this.popoverController.create({
      component: ProgressComponent,
      event: ev,
      translucent: true
    });
    return await this.popover.present();
  } 

  private popoverShow(): void {
    this.presentPopover(null);
  }

  public popoverHide(): void {
    if (this.popover) {
      this.popover.dismiss().then(() => {
        this.popover = null;
      });
    }
  }
}

interface ServerChannels {
  channels: Channel[];
}

interface ServerTvShows {
  tvshows: {
    items: TvShow[];
    total: number;
  }
}

export interface LiveChannel {
  channel: Channel;
  tvShow: TvShow;
}
