import { Injectable } from '@angular/core';
import { TvShow } from '../models/data.model';

@Injectable()

export class TvshowsService {

  public tvshows: TvShow[];

  constructor(
  ) { }

  public getTvShowsByDate(date: string): TvShow[] {
    return this.tvshows.filter(tvshow => tvshow.date === date);
  }

}
