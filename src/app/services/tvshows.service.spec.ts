/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TvshowsService } from './tvshows.service';

describe('Service: Tvshows', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TvshowsService]
    });
  });

  it('should ...', inject([TvshowsService], (service: TvshowsService) => {
    expect(service).toBeTruthy();
  }));
});
