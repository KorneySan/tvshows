import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'live', loadChildren: './live/live.module#LivePageModule' },
  { path: 'about', loadChildren: './about/about.module#AboutPageModule' },  { path: 'dates', loadChildren: './dates/dates.module#DatesPageModule' },
  { path: 'tvshows', loadChildren: './tvshows/tvshows.module#TvshowsPageModule' },
  { path: 'video', loadChildren: './video/video.module#VideoPageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
