import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TvshowComponent } from 'src/app/components/tvshow/tvshow.component';
import { TimePipe } from 'src/app/pipes/time.pipe';
import { ProgressComponent } from 'src/app/components/progress/progress.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  declarations: [
    TvshowComponent,
    TimePipe,
    ProgressComponent
  ],
  exports: [
    TvshowComponent,
    TimePipe,
    ProgressComponent
  ]
})
export class SharedComponentsModule { }
