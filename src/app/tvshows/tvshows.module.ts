import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TvshowsPage } from './tvshows.page';
import { ShortdatePipe } from '../pipes/shortdate.pipe';
//import { TvshowComponent } from '../components/tvshow/tvshow.component';
//import { TimePipe } from '../pipes/time.pipe';
import { NotFoundModule } from '../components/not-found/not-found.module';
import { SharedComponentsModule } from '../modules/shared.components/shared.components.module';

const routes: Routes = [
  {
    path: '',
    component: TvshowsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotFoundModule,
    SharedComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    TvshowsPage,
    //TvshowComponent,
    ShortdatePipe,
    //TimePipe
  ]
})
export class TvshowsPageModule {}
