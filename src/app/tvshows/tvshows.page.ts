import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TvshowsService } from '../services';
import { TvShow } from '../models/data.model';

@Component({
  selector: 'tvshows-page',
  templateUrl: './tvshows.page.html',
  styleUrls: ['./tvshows.page.scss'],
})
export class TvshowsPage implements OnInit {

  public tvShows: TvShow[] = [];
  public aDate: string;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private tvshowsService: TvshowsService,
  ) { }

  ngOnInit() {
    //
    this.activatedRoute.queryParams.subscribe(params => {
      this.aDate = params['date'];
      if (this.aDate) {
        this.tvShows = this.tvshowsService.getTvShowsByDate(this.aDate);
        //console.log(this.tvShows);
      }
    });
  }

  public get isEmpty(): boolean {
    return 0 == this.tvShows.length;
  }
}
