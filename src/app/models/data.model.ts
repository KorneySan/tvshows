export interface Channel {
    channel_id: number;
    name: string;
    logo: string;
    genres: number[];
    age_rating: string;
    available: boolean;
    stream_url: string;
}

export interface TvShow {
    channel_id: number;
    date: string;
    deleted: boolean;
    start: number;
    stop: number;
    title: string;
    ts: number;
    tvshow_id: string;
    video_id: number;
}
