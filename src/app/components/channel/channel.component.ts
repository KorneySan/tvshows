import { Component, OnInit, Input } from '@angular/core';
import { Channel } from '../../models/data.model';
import { Router } from '@angular/router';

@Component({
  selector: 'channel',
  templateUrl: 'channel.component.html',
  styleUrls: ['channel.component.scss']
})
export class ChannelComponent implements OnInit {

  @Input() channel: Channel;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public selectChannel(): void {
    this.router.navigate(['/dates'], {queryParams: {id: this.channel.channel_id}});
  }

}
