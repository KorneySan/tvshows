import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Events } from '@ionic/angular';

@Component({
  selector: 'progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss'],
})
export class ProgressComponent implements OnInit, OnDestroy {

  @Input() progressValue: number = 0;
  @Input() progressTitle: string = 'Загрузка...';

  constructor(
    private events: Events
  ) { }

  ngOnInit() {
    this.events.subscribe('progressUpdate', value => {
      this.progressValue = value;
    });
  }

  ngOnDestroy(): void {
    this.events.unsubscribe('progressUpdate');
  }
}
