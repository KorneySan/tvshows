import { Component, OnInit, Input, AfterContentInit, OnDestroy } from '@angular/core';
import { TvShow, Channel } from 'src/app/models/data.model';
import * as moment from 'moment';
import { DataService } from 'src/app/services';
import { Router } from '@angular/router';

@Component({
  selector: 'tvshow',
  templateUrl: './tvshow.component.html',
  styleUrls: ['./tvshow.component.scss'],
})
export class TvshowComponent implements OnInit, AfterContentInit, OnDestroy {

  @Input() aTvShow: TvShow = null;
  @Input() channel: Channel;
  @Input() getCurrent: boolean = false;
  public progress: number = 0;
  private readonly timeInterval = 1000;
  private timer: any;
  public showItem: boolean = true;
  public isLoadingTvShow: boolean = false;

  constructor(
    private dataService: DataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.startTimer();
  }

  ngAfterContentInit(): void {
    if (this.isCurrent) {
      setTimeout(() => {
        const active = document.getElementsByClassName('tvshow_active')[0];
        //console.log(active);
        active.scrollIntoView({block: 'center', behavior: 'smooth'});
      }, 0);
    }
    if (!this.aTvShow && this.getCurrent) this.getCurrentTvShow();
  }

  ngOnDestroy(): void {
    this.stopTimer();
  }

  public watch(): void {
    if (this.channel) {
      if (!this.aTvShow && this.getCurrent) this.getCurrentTvShow();
      //this.router.navigate(['/video'], {queryParams: {streamUrl: this.channel.stream_url}});
    }
    else {
      let channel: Channel = this.dataService.channels.find(channel => {
        return channel.channel_id == this.aTvShow.channel_id;
      });
      if (channel) this.router.navigate(['/video'], {queryParams: {streamUrl: channel.stream_url}});
    }
  }

  public get isCurrent(): boolean {
    if (this.aTvShow) return moment().isBetween(moment.unix(this.aTvShow.start), moment.unix(this.aTvShow.stop));
    else return false;
  }

  private calculateProgress(currentTime: number): void {
    if (this.aTvShow) this.progress = (currentTime - this.aTvShow.start)/(this.aTvShow.stop - this.aTvShow.start);
  }

  private startTimer(): void {
    this.stopTimer();
    if (this.isCurrent) {
      this.timer = setInterval(() => {
        this.calculateProgress(moment().unix());
        if (!this.isCurrent) {
          this.stopTimer();
          if (this.getCurrent) this.getCurrentTvShow();
        }
      }, this.timeInterval);
    }
  }

  private stopTimer(): void {
    if (this.timer) {
      clearInterval(this.timer);
      this.timer = null;
    }        
  }

  private getCurrentTvShow(): void {
    if (this.channel) {
      const currentTime = moment().unix();
      this.isLoadingTvShow = true;
      this.dataService.getCurrentTvShowById(this.channel.channel_id, currentTime).then(tvshow => {
        if (tvshow) {
          this.aTvShow = tvshow;
          this.startTimer();
          //console.log(tvshow);
        }
        else {
          this.showItem = false;
          //console.log(this.channel.name, '- пусто');
        }
        this.isLoadingTvShow = false;
      });
    }
  }

}
