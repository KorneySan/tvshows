import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
})
export class NotFoundComponent implements OnInit {

  @Input() cardTitle: string;
  @Input() cardContent: string;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {}

  public goHome(): void {
    this.router.navigate(['/home']);
  }

}
