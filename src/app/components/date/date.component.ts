import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import * as moment from 'moment';

@Component({
  selector: 'date',
  templateUrl: 'date.component.html',
  styleUrls: ['date.component.scss']
})
export class DateComponent implements OnInit, AfterViewInit {

  @Input() aDate: string;

  public static readonly DATE_FORMAT = 'dd MMMM yyyy, dddd';

  constructor(private router: Router) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    if (this.isCurrent) {
      setTimeout(() => {
        const active = document.getElementsByClassName('date_active')[0];
        //console.log(active);
        active.scrollIntoView({block: 'center', behavior: 'smooth'});
      }, 0);
    }
  }

  public selectDate(): void {
    this.router.navigate(['/tvshows'], {queryParams: {date: this.aDate}});
  }

  public get isCurrent(): boolean {
    //console.log(this.aDate);
    return moment(this.aDate).isSame(moment().format('YYYY-MM-DD'));
  }
}
