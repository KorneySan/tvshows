import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Hls from 'hls.js';

@Component({
  selector: 'videopage',
  templateUrl: './video.page.html',
  styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit, OnDestroy {

  private hls: Hls = null;
  private video: HTMLVideoElement = null;
  public buttonText: string = 'Воспроизвести';

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      const streamUrl = params['streamUrl'];
      //if (streamUrl) {
        console.log(streamUrl);
        this.video = <HTMLVideoElement>document.getElementById('video');
        if (Hls.isSupported()) {
          let hls = new Hls({
            startLevel: 2,
            capLevelToPlayerSize: true
          });
          // bind them together
          hls.attachMedia(this.video);
          // MEDIA_ATTACHED event is fired by hls object once MediaSource is ready
          hls.on(Hls.Events.MEDIA_ATTACHED, function () {
            console.log("video and hls.js are now bound together !");
            hls.loadSource('https://video-dev.github.io/streams/x36xhzz/x36xhzz.m3u8');
            hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
              console.log("manifest loaded, found " + data.levels.length + " quality level");
              this.video.play();
            });          
          });
        }      
      //}
    });
  }

  ngOnDestroy(): void {
    if (this.video) {
      this.video.pause();
    }
    if (this.hls) {
      this.hls.destroy();
    }
  }

  public onClick(): void {
    if (this.video.paused) {
      this.buttonText = 'Приостановить';
      this.video.play();
    }
    else {
      this.buttonText = 'Воспроизвести';
      this.video.pause();
    }
  }

  public goHome(): void {
    this.router.navigate(['/home']);
  }
}
