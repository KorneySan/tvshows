import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { LivePage } from './live.page';
//import { TvshowComponent } from '../components/tvshow/tvshow.component';
//import { TimePipe } from '../pipes/time.pipe';
import { SharedComponentsModule } from '../modules/shared.components/shared.components.module';
import { NotFoundModule } from '../components/not-found/not-found.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedComponentsModule,
    NotFoundModule,
    RouterModule.forChild([
      {
        path: '',
        component: LivePage
      }
    ])
  ],
  declarations: [
    LivePage,
    //TvshowComponent,
    //TimePipe
  ]
})
export class LivePageModule {}
