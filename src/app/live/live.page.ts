import { Component, OnInit } from '@angular/core';
import { DataService } from '../services';
import { LiveChannel } from '../services/data.service';

@Component({
  selector: 'live',
  templateUrl: 'live.page.html',
  styleUrls: ['live.page.scss'],
})
export class LivePage implements OnInit {

  public liveChannels: LiveChannel[] = [];
  private loading: boolean = false;

  constructor(
    private dataService: DataService
  ) {};

  ngOnInit() {
    this.loading = true;
    /*
    this.dataService.getLiveChannels(this.dataService.channels).then(result => {
      if (result) this.liveChannels = result;
      else this.liveChannels = [];
      //
      this.dataService.loadingHide();
      this.dataService.popoverHide();
      this.loading = false;
      console.log("lc = ", this.liveChannels.length);
    });
    */
   this.dataService.getChannelsImmediately();
   this.dataService.loadingHide();
}

  public get isEmpty(): boolean {
    return 0 == this.liveChannels.length;
  }

}
