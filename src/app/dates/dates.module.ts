import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DatesPage } from './dates.page';
import { DateComponent } from '../components/date/date.component';
import { MyDatePipe } from '../pipes/my-date.pipe';
import { NotFoundModule } from '../components/not-found/not-found.module';

const routes: Routes = [
  {
    path: '',
    component: DatesPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotFoundModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    DatesPage,
    DateComponent,
    MyDatePipe
  ]
})
export class DatesPageModule {}
