import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService, TvshowsService } from '../services';
import { TvShow } from '../models/data.model';
import * as moment from 'moment';
//import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'dates',
  templateUrl: 'dates.page.html',
  styleUrls: ['dates.page.scss'],
})
export class DatesPage implements OnInit {

  //private tvShows: TvShow[] = [];
  public uniqueDates: string[] = [];
  private loading: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private dataService: DataService,
    private tvshowsService: TvshowsService//,
    //private loadingController: LoadingController
    ) { }

  ngOnInit() {
    moment.updateLocale('ru', {
      months : {
          format: 'января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря'.split('_'),
          standalone: 'январь_февраль_март_апрель_май_июнь_июль_август_сентябрь_октябрь_ноябрь_декабрь'.split('_')
      }
    });
    //
    this.activatedRoute.queryParams.subscribe(params => {
      const id = params['id'];
      if (id) {
        /*
        this.presentLoading();
        setTimeout(() => {

        }, 300);
        */
        this.loading = true;
        this.dataService.getTvShowsById(+id, 'Загрузка дат и передач...').then(result => {
          //console.log(result);
          //this.tvShows = result;
          this.tvshowsService.tvshows = result;
          this.uniqueDates = this.getUniqueDates();
          //
          /*
          this.loading.dismiss();
          this.loading = null;
          */
          this.dataService.loadingHide();
          this.loading = false;
        });
      }
    });
  }

  public getUniqueDates(): string[] {
    /*
    const uniqueDates: string[] = this.tvShows.map(tvshow => tvshow.date).filter((date, index, array)=>{
      return array.indexOf(date) == index;
    }).sort();
    */
   const uniqueDates: string[] = this.tvshowsService.tvshows.map(tvshow => tvshow.date).filter((date, index, array)=>{
    return array.indexOf(date) == index;
  }).sort();
  //console.log(uniqueDates);
    return uniqueDates;
  }

  public get isEmpty(): boolean {
    return 0 == this.uniqueDates.length;
  }
  /*
  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: "Загрузка дат..."
    });
    await this.loading.present();
  }
  */
}
