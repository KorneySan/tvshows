import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'myDate'
})
export class MyDatePipe implements PipeTransform {

  transform(value: string): string {
    moment.locale('ru');
    const data=moment(value);
    /*
    const day=moment(data).format('DD');
    const month=moment(data).format('MMMM');
    const dayofweek=moment(data).format('dddd');
    */
    //return day+' '+month+', '+dayofweek;
    return moment(data).format('DD MMMM, dddd');
  }

}
