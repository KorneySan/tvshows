import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'shortdate'
})
export class ShortdatePipe implements PipeTransform {

  transform(value: string): string {
    moment.locale('ru');
    const data=moment(value);
    return moment(data).format('DD MMM, dd');
  }

}
